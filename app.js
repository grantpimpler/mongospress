const express = require("express");
const bodyParser = require("body-parser");
const cookieSession = require("cookie-session");
const get = require("lodash/get");
const morgan = require("morgan");
const mongoose = require("mongoose");
mongoose.set("useCreateIndex", true);

const router = express.Router();

const MONGO_PORT = 27017;
const MONGO_DATABASE = 'mongospress';

// TODO: Move this to an environment variable and use your own
process.env.JWT_KEY = "S0m3H@rdToCr@ckS@ltyToInt3rM!ngl3";

const mongoOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true
};
mongoose.connect(`mongodb://localhost:${MONGO_PORT}/${MONGO_DATABASE}`, mongoOptions);

// Setup the application
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(morgan("dev"));
app.use(
  cookieSession({
    name: "mongospress-session",
    keys: ["vueauthrandomkey"],
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
  })
);
app.use("/api", router);

// Setup routes modules
const userRoutes = require("./api/routes/users");
router.use("/user", userRoutes);

app.use((req, res, next) => {
    const error = new Error("Not Found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
res.status(error.status || 500);
res.json({
        error: {
        message: error.message || "An error occurred."
        }
    });
});

module.exports = app;