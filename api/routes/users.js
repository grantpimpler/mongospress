const express = require("express");
const router = express.Router();
const checkAuth = require("../middleware/check-auth");
const UserController = require("../controllers/users");

router.get("/:id", checkAuth, UserController.user_by_id);

router.post("/create", UserController.user_create);

router.post("/login", UserController.user_login);

router.patch("/:id", checkAuth, UserController.user_update_by_id);

router.delete("/:id", checkAuth, UserController.user_delete_by_id);

module.exports = router;
