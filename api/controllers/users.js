const User = require("../models/user");
const get = require("lodash/get");
const assign = require("lodash/assign");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const TOKEN_EXPIRATION = "1h";

removeSensitiveInfo = (response) => {
  delete response["password"];
  delete response["__v"];
  return response;
}

/**
 * Get a user by id. This will also delete the password field out of the response so that we do not expose the salted and hashed password information.
 */
exports.user_by_id = (req, res) => {
  User.findById(req.params.id)
    .exec()
    .then(user => {
      let response = assign({}, user._doc);
      res.status(200).json(removeSensitiveInfo(response));
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

/**
 * The user create service creates a new user.  The main authentication in the creation process is via email which must match a valid email string and it must also be unique to the system. Any non-unique email address or an invalid email string will trigger an error.  The response of this service will also return a token back to the client that will be used in any subsequent call that requires authentication.  Not all calls necessarily need to require it in the case of a product gallery or other services where you want that data to be available to anyone; however, any services calls that need to be locked down should use that token in the Authorization header. 
 */
exports.user_create = (req, res) => {
  let body = get(req, ["body"], null);
  if (body) {
    User.find({ email: req.body.email })
      .exec()
      .then(user => {
        if (user.length > 0) {
          return res.status(409).json({
            message: "Email already exists."
          });
        } else {
          bcrypt.hash(body.password, 10, (err, hash) => {
            if (err) {
              return res.status(500).json({ error: err });
            } else {
              const user = new User({
                _id: new mongoose.Types.ObjectId(),
                active: true,
                created: new Date().toUTCString(),
                email: body.email,
                firstName: body.firstName,
                lastName: body.lastName,
                password: hash
              });
              user
                .save()
                .then(result => {
                  let response = assign({}, result._doc);
                  const token = jwt.sign(
                    { email: result.email, userId: result._id },
                    process.env.JWT_KEY,
                    { expiresIn: TOKEN_EXPIRATION }
                  );
                  res.status(200).json(removeSensitiveInfo(response));
                })
                .catch(err => {
                  console.log("goal err", err);
                  res.status(500).json({ error: err });
                });
            }
          });
        }
      })
      .catch(err => {
        console.log("goal err", err);
        res.status(500).json({ error: err });
      });
  } else {
    res
      .status(412)
      .json({ error: `No data was passed to the add behavior call.` });
  }
};

exports.user_login = (req, res, next) => {
  User.find({ email: req.body.email })
    .exec()
    .then(user => {
      if (user.length < 1) {
        return res.status(401).json({
          message: "Auth Failed"
        });
      } else {
        bcrypt.compare(req.body.password, user[0].password, (err, result) => {
          if (result) {
            const token = jwt.sign(
              { email: user[0].email, userId: user[0]._id },
              process.env.JWT_KEY,
              { expiresIn: TOKEN_EXPIRATION }
            );
            let response = assign({}, get(user[0], ["_doc"], null));
            response["token"] = token;
            return res.status(200).json(removeSensitiveInfo(response));
          } else {
            return res.status(401).json({
              message: "Auth Failed"
            });
          }
        });
      }
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};

exports.user_update_by_id = (req, res) => {
  const id = get(req, ["params", "id"], null);
  if (id) {
    const updateOps = {};
    for (const ops of req.body) {
      updateOps[ops.propName] = ops.value;
    }
    User.updateOne({ _id: id }, { $set: updateOps })
      .exec()
      .then(result => {
        const count = get(result, "nModified", 0);
        res.status(200).json({ message: `Updated ${count} records.` });
      })
      .catch(err => {
        console.log(err);
        res.status(500).json({ error: err });
      });
  }
};

exports.user_delete_by_id = (req, res) => {
  const id = get(req, ["params", "id"], null);
  User.deleteOne({ _id: id })
    .exec()
    .then(result => {
      const count = get(result, "deletedCount", 0);
      res.status(200).json({ message: `Deleted ${count} records.` });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
};
