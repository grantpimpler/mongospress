const mongoose = require("mongoose");

/**
 * Items to note:
 * - email is a required field that is checked for uniqueness and it also runs the input through an email regular expression validation
 * - the collection options tells us in what collection in the database that the user will be stored.
 */
const userSchema = mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    active: Boolean,
    created: String,
    email: {
      type: String,
      required: true,
      unique: true,
      match: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
    },
    firstName: String,
    lastName: String,
    password: String
  },
  { collection: "users" }
);

module.exports = mongoose.model("User", userSchema);
