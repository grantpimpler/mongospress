# mongospress
Jumpstarter NodeJS/Express/Mongo and REST API server setup

## Overview
This is a skeleton server application based on Node.js, Express, & MongoDb.  The architecture is based on the most excellent REST videos on Youtube by Academind (Maximillian Schwarzmuller) [Creating a RESTful API with NodeJS](https://www.youtube.com/watch?v=0oXYLzuucwE "Video #1"). I highly recommend watching the full series (14 videos) as Maximillian presents a ton of great commentary on building a REST API as he goes along. Taught this old dog a few new tricks. This repo is just a quick jumpstart on a REST API setup to get you up and running as quickly as possible.  Feel free to extend this and submit Pull Requests if you so desire.

## Assumptions
* You have mongodb installed and running.  **This project will not install mongo or start it up for you. It assumes you already have this in place.**

## Prerequisites
* Node 12+
* Express
* MongoDb
* Mongoose

## Setup
Go ahead and do a git clone on the project or fork it for your own purposes.

```
$ git clone https://grantpimpler@bitbucket.org/grantpimpler/mongospress.git
```

If you intend to submit a Pull Request for changes, you will need to create a feature (or Bugfix) branch to do your development in. Once you are done and ready for re-integration, submit a Pull Request to be reviewed.

Once you have the project locally, you will need to do an npm install.  This has been tested with Node 10+.  No guarantees on anything lower than 10. You probably shouldn't be starting a new project with anything less than that anyway.

```javascript
npm install
```


### Basic Mongo Setup (app.js)
There are two constants for Mongo.  The first is ```MONGO_PORT``` which is set to the default port Mongo runs on (27017). If you did not change the port, you should be good to go. If you are using a different port, please update this constant. The second is the ```MONGO_DATABASE``` which points to the database (collection) that we are going to use inside of Mongo.

```javascript
const MONGO_PORT = 27017;
const MONGO_DATABASE = 'mongospress';
```
The two constants above create the path to the database:
```javascript
mongoose.connect(`mongodb://localhost:${MONGO_PORT}/${MONGO_DATABASE}`, mongoOptions);
```
If you are using AWS or some non-localhost instance of Mongo, you will want to provide the full path to the database and those two constants are not important if you feed it the full path.

### Environment Setup
**Ideally**, you should create an environment variable for the ```JWT_KEY``` rather than storing it in the code in this way. Once you have an environment variable up and running named ```JWT_KEY``` you can get rid of the line below.  

```javascript
// TODO: Move this to an environment variable and use your own or suffer the consequences.  ;)
process.env.JWT_KEY = "S0m3H@rdToCr@ckS@ltyToInt3rM!ngl3";
```

## Start the server (server.js)
The server boots up on either an environment variable set to ```PORT``` or ```3050```.  You can obviously change that to any port that you like.
* npm run serve

```javascript
const port = process.env.PORT || 3050;
```

If the server is running successfully, you should see the following output in the console (listing your port info):

```
Mongospress app listening on port 3050!
```


## Service authentication
Any services that need to be locked down for an authenticated session should use a service header where ```{{token}}``` is the token provided either from the user creation service or from the user login service (upon successful login).

```Authorization Bear {{token}}```

Also of note, the token is generated in the user creation or login services.  There is a const defined to indicate how long the json web token is good for. See the jsonwebtoken documentation for the appropriate values.

```javascript
const TOKEN_EXPIRATION = "1h";
```
